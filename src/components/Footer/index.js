import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Grid, Icon } from 'semantic-ui-react';
import { TABS } from '../../utils/constants';

const style = {
    footer: {
        backgroundColor: 'white',
    },
    icon: {
        position: 'relative',
    },
    notification: {
        position: 'absolute',
        right: 0,
        top: 0,
        width: 8,
        height: 8,
        borderRadius: 8,
        backgroundColor: '#1db954',
    },
}

class Footer extends PureComponent {

    onTabClick = (iconActive) => {
        const { onClickTab } = this.props;

        onClickTab(iconActive);
    };

    getActiveTabClass = (state) => {
        const { iconActive } = this.props;
        const active = (iconActive === state) ? 'active' : '';

        return `footerItem ${active}`;
    };

    render() {
        const {
            HOME, OPENING, APPLICATIONS, PROFILE,
        } = TABS;

        return (
            <Grid columns={4} style={style.footer}>
                <Grid.Row className="footer">
                    <Grid.Column
                        className={this.getActiveTabClass(HOME)}
                        textAlign="center"
                        mobile={4}
                        verticalAlign="middle"
                        onClick={() => this.onTabClick(HOME)}
                    >
                        <Icon
                            name="home"
                            size="large"
                            style={style.icon}
                        />
                        <div>
                        Home
                        </div>
                    </Grid.Column>
                    <Grid.Column
                        className={this.getActiveTabClass(OPENING)}
                        textAlign="center"
                        mobile={4}
                        verticalAlign="middle"
                        onClick={() => this.onTabClick(OPENING)}
                    >
                        <Icon
                            name="briefcase"
                            size="large"
                            style={style.icon}
                        >
                            <div style={style.notification}>
                            </div>
                        </Icon>
                        <div>
                        Openings
                        </div>
                    </Grid.Column>
                    <Grid.Column
                        className={this.getActiveTabClass(APPLICATIONS)}
                        textAlign="center"
                        mobile={4}
                        verticalAlign="middle"
                        onClick={() => this.onTabClick(APPLICATIONS)}
                    >
                        <Icon
                            name="file outline"
                            size="large"
                            style={style.icon}
                        >
                            <div style={style.notification}>
                            </div>
                        </Icon>
                        <div>
                        Applications
                        </div>
                    </Grid.Column>
                    <Grid.Column
                        className={this.getActiveTabClass(PROFILE)}
                        textAlign="center"
                        mobile={4}
                        verticalAlign="middle"
                        onClick={() => this.onTabClick(PROFILE)}
                    >
                        <Icon
                            name="user circle"
                            size="large"
                            style={style.icon}
                        >
                            <div style={style.notification}>
                            </div>
                        </Icon>
                        <div>
                        Profile
                        </div>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

Footer.propTypes = {
    onClickTab: PropTypes.func,
    iconActive: PropTypes.string,
};

Footer.defaultProps = {
    onClickTab: () => {},
    iconActive: TABS.HOME,
};

export default Footer;
