import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Container } from 'semantic-ui-react';
import Nav from '../Nav';
import Sidebar from '../Nav/sidebar';
import Side from '../Nav/side';
import { isMobile } from '../../utils/helper';

const styles = {
    container: {},
    footer: {
        position: 'fixed',
        bottom: '0px',
    },
};

class Screen extends PureComponent {
    constructor(props) {
        super(props);
        this.state = { openSideBar: false };
    }

    componentDidMount() {

    }

    onToggleSideBar = (open) => {
        this.setState({
            openSideBar: open,
        });
    }

    renderSide = () => {
        const { openSideBar } = this.state;

        return (
            <Side
                onToggleSidebar={this.onToggleSideBar}
                openSideBar={openSideBar}
            />
        );
    }

    renderNav = () => {
        const { openSideBar } = this.state;
        const { logout, isAuthenticated } = this.props;

        return (
            <Nav
                onToggleSidebar={this.onToggleSideBar}
                openSideBar={openSideBar}
                logout={logout}
                isAuthenticated={isAuthenticated}

            />
        );
    }

    renderSideBar = () => {
        const { openSideBar } = this.state;

        return (
            <Sidebar
                sidebar={this.renderSide()}
                sidebarOpen={openSideBar}
                onClose={this.onToggleSideBar}
            />
        );
    }


    render() {
        const { content, footer, isAuthenticated } = this.props;

        return (
            <div style={styles.container}>
                {this.renderNav()}
                {isAuthenticated && (
                    <div style={{
                        position: 'relative',
                        height: '100vh',
                    }}
                    >
                        <div>
                            <div style={{
                                width: !isMobile() && '60%',
                                margin: 'auto',
                                paddingBottom: '5em',
                            }}
                            >
                                {content}
                            </div>
                            <div style={styles.footer}>
                                {footer}
                            </div>
                        </div>
                        {this.renderSideBar()}
                    </div>
                )}
            </div>
        );
    }
}

Screen.propTypes = {
    headerText: PropTypes.string,
    shouldBack: PropTypes.bool,
    content: PropTypes.objectOf(PureComponent),
    footer: PropTypes.objectOf(PureComponent),
    isAuthenticated: PropTypes.bool.isRequired,

};

Screen.defaultProps = {
    headerText: '',
    shouldBack: false,
    content: null,
    footer: null,
};

const mapStateToProps = (state) => (state);

export default connect(mapStateToProps, (dispatch, state) => ({
    actions: bindActionCreators({}, dispatch),
}))(Screen);
