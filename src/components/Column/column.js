import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Grid } from 'semantic-ui-react';

const styles = {
    grid: {
        marginTop: '1em',
    },
};

export default class Column extends PureComponent {
    render() {
        const {
            // eslint-disable-next-line react/prop-types
            mobile, tablet, computer, style, children,
        } = this.props;
        return (
            <Grid.Column
                mobile={mobile}
                tablet={tablet}
                computer={computer}
                style={{
                    ...styles.grid,
                    ...style,
                }}
            >
                {/* eslint-disable-next-line react/prop-types */}
                {children}
            </Grid.Column>
        );
    }
}

Column.defaultProps = {
    mobile: 16,
    tablet: 8,
    computer: 8,
    style: {},
};

Column.propTypes = {
    mobile: PropTypes.number,
    tablet: PropTypes.number,
    computer: PropTypes.number,
    style: PropTypes.objectOf({}),
};
