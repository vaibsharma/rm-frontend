import React, { PureComponent } from 'react';
import { Grid } from 'semantic-ui-react';

export default class RMRow extends PureComponent {
    render() {
        return (
            <Grid.Row style={{
                borderWidth: '0.01em',
                borderStyle: 'solid',
                borderColor: 'rgba(0,0,0,0.1)',
            }}
            >
                {this.props.children}
            </Grid.Row>
        );
    }
}
