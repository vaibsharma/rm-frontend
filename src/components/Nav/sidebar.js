import React from 'react';
import Sidebar from 'react-sidebar';
import { isMobile } from '../../utils/helper';

class SideNav extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            style: {
                root: {
                    zIndex: -1,
                },
            },
        };
    }
    componentDidMount() {
        const { onClose } = this.props;

        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
        document.getElementsByClassName('slideBarBackground')[0].addEventListener('click', () => {
            onClose(false);
        });
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (this.props.sidebarOpen != nextProps.sidebarOpen) {
            if (nextProps.sidebarOpen) {
                this.setState({
                    style: {
                        content: {
                            backgroundColor: 'rgba(0, 0, 0, 0.3)',
                        },
                    },
                });
            } else {
                setTimeout(() => {
                    this.setState({
                        style: {
                            root: {
                                zIndex: -1,
                            },
                        },
                    });
                }, 250);
            }
        }
    }

    onSetSidebarOpen(open) {
        // this.setState({ sidebarOpen: open });
    }

    render() {
        const { sidebarOpen, sidebar, content } = this.props;
        const { style: sideBarStyles } = this.state;

        return (
            <Sidebar
                sidebar={sidebar}
                open={sidebarOpen}
                docked={sidebarOpen}
                onSetOpen={this.onSetSidebarOpen}
                touch={false}
                contentClassName="slideBarBackground"
                sidebarClassName="sideBarContent"
                styles={sideBarStyles}
            >
                {content}
            </Sidebar>
        );
    }
}

export default SideNav;
