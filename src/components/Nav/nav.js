import React, { Component, PureComponent } from 'react';
import {
    Menu, Segment, Header, Icon,
} from 'semantic-ui-react';
import propTypes from 'prop-types';
import { isMobile } from '../../utils/helper';

export default class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = { activeItem: '' };
    }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    toggleSideBar = () => {
        const { onToggleSidebar, openSideBar } = this.props;

        if (typeof (onToggleSidebar) === 'function') {
            onToggleSidebar(!openSideBar);
        }
    }


    render() {
        const headerSize = isMobile() ? 'small' : 'medium';
        const { withoutIcon, shouldCenter, isAuthenticated } = this.props;
        let headerCenterStyle = {};
        if (shouldCenter) {
            headerCenterStyle = {
                margin: 'auto',
            };
        }

        return (
            <div>
                <Segment inverted style={{ marginBottom: '0em' }} className="noborder">
                    <Menu
                        inverted
                        secondary
                    >
                        <Menu.Item
                            style={headerCenterStyle}
                        >
                            <Header size={headerSize} inverted>
                                {!isMobile() && !withoutIcon && (
                                    <Icon inverted color="grey" name="student" />
                                )}
                                {isMobile() && !withoutIcon && (
                                    <Icon
                                        inverted
                                        color="grey"
                                        name="bars"
                                        onClick={this.toggleSideBar}
                                    />
                                )}
                            Placement Manager
                            </Header>
                        </Menu.Item>
                        {
                            isAuthenticated && (
                                <Menu.Item
                                    active
                                    name="reviews"
                                    onClick={this.props.logout}
                                    position="right"
                                >
                            Logout
                                </Menu.Item>
                            )
                        }
                    </Menu>
                </Segment>

                <div style={{}}>
                    {this.props.children}
                </div>

            </div>
        );
    }
}

Nav.propTypes = {
    openSideBar: propTypes.bool,
    onToggleSidebar: propTypes.func,
    shouldCenter: propTypes.bool,
    withoutIcon: propTypes.bool,
    children: propTypes.objectOf(PureComponent),
};

Nav.defaultProps = {
    shouldCenter: false,
    withoutIcon: false,
    children: undefined,
    openSideBar: false,
    onToggleSidebar: () => {},
};
