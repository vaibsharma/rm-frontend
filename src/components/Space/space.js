import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

const styles = {
    space: {
        height: '1em',
    },
    separate: {
        borderBottomStyle: 'solid',
        borderBottomWidth: '0.5px',
        borderBottomColor: '#BDBDBD',
        marginLeft: '10px',
        marginRight: '10px',
        marginBottom: '10px',
    },
};

export default class Space extends PureComponent {
    render() {
        // eslint-disable-next-line react/prop-types
        const {
            style, children, height, separate,
        } = this.props;
        return (
            <div>
                <div style={{
                    ...styles.space,
                    ...style,
                    height,
                }}
                >
                    {children}
                </div>
                {separate && (
                    <div style={styles.separate} />
                )}
            </div>
        );
    }
}

Space.propTypes = {
    height: PropTypes.string,
    separate: PropTypes.bool,
};

Space.defaultProps = {
    style: {},
    height: '1em',
    separate: false,
};

Space.propTypes = {
    style: PropTypes.objectOf(Object),
};
