import React, { PureComponent } from 'react';
import { Image, Icon } from 'semantic-ui-react';

const style = {
    container: {
        height:'110px',
        width: '110px',
        marginLeft: '10px',
        position: 'relative',
    },
    image: {
        height: '100px',
        width: '100px',
        borderRadius: '100px',
        position: 'absolute',
        top: '5px',
        left: '5px',
    },
    icon: {
        color: '#ffffff',
        fontSize: '20px',
        verticalAlign: 'middle',
        paddingTop: '1px',
        paddingLeft: '1px',
    },
    iconContainer: {
        height: '24px',
        width: '24px',
        borderRadius: '22px',
        backgroundColor: '#0288D1',
        position: 'absolute',
        bottom: '5px',
        right: '5px',
    },
};

export default class ProfilePicture extends PureComponent {

    render() {
        return (
            <div style={style.container}>
                <Image
                    style={style.image}
                    src="http://localhost:8000/media/avatar/44667288_2194328027492536_895153504927368221_n.jpg"
                    circular
                />
                <div style={style.iconContainer}>
                    <Icon style={style.icon} name="plus" />
                </div>
            </div>
        );
    }
}
