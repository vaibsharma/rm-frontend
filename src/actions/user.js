import requests from '../utils/requests';

const USER_STATE = {
    INIT: 'USER_INIT',
    SUCCESSFUL: 'USER_SUCCESSFUL',
    FAILED: 'USER_FAILED',
};

export const getProfile = (username = '') => (dispatch) => {
    dispatch({ type: USER_STATE.INIT, data: {} });
    requests.get(`http://127.0.0.1:8000/user/profile/${username}`).then((data) => {
        dispatch({
            type: USER_STATE.SUCCESSFUL,
            data,
        });
    }).catch((error) => {
        dispatch({
            type: USER_STATE.FAILED,
            data: {
                error,
            },
        });
    });
};

export default {
    state: USER_STATE,
    actions: {
        getProfile,
    }
};
