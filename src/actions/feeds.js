import requests from '../utils/requests';

const HOME_FEEDS = {
    INIT: 'HOME_INIT',
    SUCCESSFUL: 'HOME_SUCCESSFUL',
    FAILED: 'HOME_FAILED',
};

export const getHomeFeed = () => (dispatch) => {
    dispatch({ type: HOME_FEEDS.INIT, data: [] })
    requests.get('http://127.0.0.1:8000/university/user_feeds')
        .then((data) => {
            dispatch({ type: HOME_FEEDS.SUCCESSFUL, data });
        })
        .catch((error) => {
            dispatch({ type: HOME_FEEDS.FAILED, error });
        });
}

export default {
    state: HOME_FEEDS,
    actions: {
        getHomeFeed,
    }
};
