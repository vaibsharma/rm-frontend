import requests from '../utils/requests';

export const OPENING_FEEDS = {
    INIT: 'OPENING_INIT',
    SUCCESSFUL: 'OPENING_SUCCESSFUL',
    FAILED: 'OPENING_FAILED',
};

export const APPLY_OPENING = {
    INIT: 'APPLY_INIT',
    SUCCESSFUL: 'APPLY_SUCCESSFUL',
    FAILED: 'APPLY_FAILED',
};

export const getOpenings = () => (dispatch) => {
    dispatch({
        type: OPENING_FEEDS.INIT,
        data: [],
    });
    requests.get('http://127.0.0.1:8000/university/openings')
        .then((data) => {
            dispatch({
                type: OPENING_FEEDS.SUCCESSFUL,
                data,
            });
        })
        .catch((error) => {
            dispatch({
                type: OPENING_FEEDS.FAILED,
                error,
            });
            console.error(error);
        });
};

export const applyOpening = (openingId) => (dispatch) => {
    dispatch({
        type: APPLY_OPENING.INIT,
        data: {
            opening_id: openingId,
        },
    });
    const body = {
        opening_id: openingId,
    };
    requests.post('http://127.0.0.1:8000/company/opening/apply/', body)
        .then((data) => {
            dispatch({
                type: APPLY_OPENING.SUCCESSFUL,
                data,
            });
        }).catch((data) => {
            dispatch({
                type: APPLY_OPENING.FAILED,
                data: {
                    error: data.error,
                },
            });
        });
};
export default {
    state: OPENING_FEEDS,
    actions: {
        getOpenings,
        applyOpening,
    },
};
