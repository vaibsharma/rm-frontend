import requests from '../utils/requests';

const MY_APPLICATION_FEEDS = {
    INIT: 'MY_APPLICATION_INIT',
    SUCCESSFUL: 'MY_APPLICATION_SUCCESSFUL',
    FAILED: 'MY_APPLICATION_FAILED',
};

const UPDATE_APPLICATION_STATUS = {
    INIT: 'UPDATE_APPLICATION_STATUS_INIT',
    SUCCESSFUL: 'UPDATE_APPLICATION_STATUS_SUCCESSFUL',
    FAILED: 'UPDATE_APPLICATION_STATUS_FAILED',
};

export const updateApplicationStatus = (applicationId, status) => (dispatch) => {
    dispatch({
        type: UPDATE_APPLICATION_STATUS.INIT,
        data: [],
    });

    const body = {
        application: applicationId,
        status: status
    };
    requests.post('http://127.0.0.1:8000/university/application/update/status/', body)
        .then((data) => {
            dispatch({
                type: UPDATE_APPLICATION_STATUS.SUCCESSFUL,
                data,
            });
        })
        .catch((error) => {
            dispatch({
                type: UPDATE_APPLICATION_STATUS.FAILED,
                error,
            });
        });
}

export const getMyApplications = () => (dispatch) => {
    dispatch({
        type: MY_APPLICATION_FEEDS.INIT,
        data: [],
    })
    requests.get('http://127.0.0.1:8000/university/applications')
        .then((data) => {
            dispatch({
                type: MY_APPLICATION_FEEDS.SUCCESSFUL,
                data,
            });
        })
        .catch((error) => {
            dispatch({
                type: MY_APPLICATION_FEEDS.FAILED,
                error,
            });
        });
};

export default {
    state: MY_APPLICATION_FEEDS,
    actions: {
        getMyApplications,
        updateApplicationStatus,
    },
};
