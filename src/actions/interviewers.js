import requests from '../utils/requests';

export const INTERVIEWS_LIST = {
    INIT: 'INTERVIEWS_LIST_INIT',
    SUCCESSFUL: 'INTERVIEWS_LIST_SUCCESSFUL',
    FAILED: 'INTERVIEWS_LIST_FAILED',
};

export const ASSIGN_INTERVIEW = {
    INIT: 'ASSIGN_INTERVIEW_INIT',
    SUCCESSFUL: 'ASSIGN_INTERVIEW_SUCCESSFUL',
    FAILED: 'ASSIGN_INTERVIEW_FAILED',
};

// eslint-disable-next-line max-len
export const createInterview = (applicationId, interviewerId, candidateId, interviewDesciption, dateTime) => (dispatch) => {
    dispatch({
        type: ASSIGN_INTERVIEW.INIT,
        data: [],
    });
    const body = {
        application: applicationId,
        interviewer: interviewerId,
        candidate: candidateId,
        description: interviewDesciption,
        scheduled_at: dateTime,
    };
    requests.post('http://127.0.0.1:8000/company/interview/create/', body)
        .then((data) => {
            dispatch({
                type: ASSIGN_INTERVIEW.SUCCESSFUL,
                data,
            });
        })
        .catch((error) => {
            dispatch({
                type: ASSIGN_INTERVIEW.FAILED,
                error,
            });
        });
};

export const getInterviewersList = () => (dispatch) => {
    dispatch({
        type: INTERVIEWS_LIST.INIT,
        data: [],
    });
    requests.get('http://127.0.0.1:8000/company/interviewers/')
        .then((data) => {
            dispatch({
                type: INTERVIEWS_LIST.SUCCESSFUL,
                data,
            });
        })
        .catch((error) => {
            dispatch({
                type: INTERVIEWS_LIST.FAILED,
                error,
            });
        });
};

export default {
    state: INTERVIEWS_LIST,
    actions: {
        getInterviewersList,
        createInterview,
    },
};
