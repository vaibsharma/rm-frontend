const AUTH_TYPES = {
    INIT: 'AUTH_INIT',
    SUCCESSFUL: 'AUTH_SUCCESSFUL',
    FAILED: 'AUTH_FAILED',
};

export default {
    ...AUTH_TYPES,
};
