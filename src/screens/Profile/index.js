import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Message } from 'semantic-ui-react';

import Screen from '../../components/Screen';
import Footer from '../../components/Footer';

const MessageExampleMessage = () => (
    <Message>
        <Message.Header>Changes in Service</Message.Header>
        <p>
            We updated our privacy policy here to better service our customers. We
            recommend reviewing the changes.
        </p>
    </Message>
);


class Profile extends PureComponent {

    renderContent = () => MessageExampleMessage();

    onClickWithVal = (currentTab = 'home') => {
        console.log(currentTab);
    }


    renderFooter = () => (
        <Footer
            onClickTab={this.onClickWithVal}
        />
    )

    render() {
        return (
            <Screen
                footer={this.renderFooter()}
                content={this.renderContent()}
            />
        );
    }
}

const mapStateToProps = (state) => (state);

export default connect(mapStateToProps, (dispatch, state) => ({
    actions: bindActionCreators({}, dispatch),
}))(Profile);
