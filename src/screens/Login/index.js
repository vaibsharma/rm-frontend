import React, { PureComponent, Component } from 'react';
import {
    Button, Form, Icon, Message, Container, Modal, Input,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { isMobile } from '../../utils/helper';


import Space from '../../components/Space';
import Nav from '../../components/Nav';
import AuthClient from '../../utils/auth';

let
    topMargin = {
        marginTop: '8em',
    };
if (isMobile()) {
    topMargin = {
        marginTop: '2em',
    };
}

const MessageExampleIconProp = () => (
    <Message
        icon="inbox"
        header="Your College Placement Manager"
        content="Please login to continue."
        style={{
            marginTop: '1em',
            marginBottom: '1em',
        }}
    />
);


class ModalExampleDimmer extends PureComponent {
    constructor(props) {
        super(props);
        this.state = { open: false };
    }

    close = () => {
        if (typeof this.props.onClose === 'function') {
            this.props.onClose();
        }
    }

    render() {
        const { open } = this.props;

        return (
            <Modal dimmer size="small" open={open} onClose={this.close}>
                <Modal.Header>Find Your Account</Modal.Header>
                <Modal.Content image>
                    <Modal.Description>
                        <p>
                            Please enter your email or phone number to find your account.
                        </p>
                        <Input fluid placeholder="Search..." />
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button
                        positive
                        icon="checkmark"
                        labelPosition="right"
                        content="Yep, that's me"
                        onClick={this.close}
                    />
                </Modal.Actions>
            </Modal>
        );
    }
}

class LoginForm extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            err: false,
        };
    }

    onChangeText = (key) => (event) => {
        const newState = {};
        newState[key] = event.target.value;
        this.setState(newState);
    }

    checkForNextUrl = () => {
        const url = window.location.href;
        const urlObject = new URL(url);
        const nextUrl = urlObject.searchParams.get('url');
        if (nextUrl && nextUrl.length > 0) {
            this.props.history.push(new URL(nextUrl).pathname);
        }
    }

    onSubmit = () => {
        const { email, password } = this.state;
        const { authClient } = this.props;

        if (password.length === 0) {
            this.setState({
                err: true,
            });
        } else {
            authClient.generateNewToken(email, password, (res) => {
                // console.log(res);
                console.log('successfull');
                this.checkForNextUrl();
            }, (error) => {
                // debugger;
                const { loginFailed, isLoginRequired } = error;
                this.props.history.push('/login', {
                    loginFailed,
                    isLoginRequired,
                });
            });
        }
    }

    renderMessageOnError = () => (this.state.err && (
        <div>
            <Space />
            <Message negative>
                <p>Seems like you entered a wrong Email or password</p>
            </Message>
        </div>
    ))

    render() {
        const { email, password, err } = this.state;
        const { loginFailed } = this.props;
        return (
            <div>
                <Form>
                    <Form.Field error={loginFailed}
                    >
                        <label>Email or Phone</label>
                        <input
                            label="Email or Phone"
                            placeholder="Please enter yout"
                            onChange={this.onChangeText('email')}
                            error={loginFailed}
                            value={email}
                        />
                    </Form.Field>
                    <Form.Field error={loginFailed}>
                        <label>Password</label>
                        <input
                            label="Password"
                            type="password"
                            placeholder="Last Name"
                            err={loginFailed}
                            onChange={this.onChangeText('password')}
                            value={password}
                        />
                    </Form.Field>
                    <Button primary type="submit" onClick={this.onSubmit}>Please Login</Button>
                </Form>
                {this.renderMessageOnError()}
            </div>
        );
    }
}

class FormExampleForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openModal: false,
            isLoginRequired: false,
        };
    }

    componentDidMount() {
        const { history: { location } } = this.props;
        const { state } = location;
        if (!state) return;
        const { isLoginRequired } = state;
        if (isLoginRequired) {
            this.setState({
                isLoginRequired,
            }, () => {
                if (this.state.isLoginRequired) {
                    setTimeout(() => {
                        this.setState({
                            isLoginRequired: false,
                        });
                    }, 5000);
                }
            });
        }
    }

    toggleForgotPasswordModal = (openModal) => {
        this.setState({
            openModal,
        });
    }

    onModalClose = () => {
        this.toggleForgotPasswordModal(false);
    }

    onModalOpen = () => {
        this.toggleForgotPasswordModal(true);
    }

    renderLoginRequiredMessage = () => {
        const { history } = this.props;
        const { location: { state } } = history;
        let loginFailed = false, isLoginRequired = false;
        if (state) {
            loginFailed = state.loginFailed;
            isLoginRequired = state.isLoginRequired
        }
        if(isLoginRequired) return (
            <Message negative>
                <Message.Header>Please enter your credentials!</Message.Header>
            </Message>
        );
        if(loginFailed){
            return(<Message negative>
                <Message.Header>Either username or password is incorrect!</Message.Header>
            </Message>)
        }
        return null;
    }

    renderModal = () => {
        const { openModal } = this.state;

        return (
            <ModalExampleDimmer
                open={openModal}
                onClose={this.onModalClose}
            />
        );
    }

    renderForgotPassword = () => (
        <Message attached="bottom" warning onClick={() => { this.onModalOpen(); }}>
            <Icon name="help" />
            Forgot Account
        </Message>
    )


    render() {
        const { authClient, history } = this.props;
        const { location: { state } } = history;
        let loginFailed = false
        if (state){
            loginFailed = state.loginFailed;
        }

        return (
            <Container style={{
                marginLeft: 'auto',
                marginRight: 'auto',
                width: '50vw',
                ...topMargin,
            }}
            >
                {MessageExampleIconProp()}
                <div>
                    {/* eslint-disable-next-line max-len */}
                    <LoginForm
                        authClient={authClient}
                        history={history}
                        loginFailed={loginFailed}
                    />
                    <Space />
                    {this.renderForgotPassword()}
                    {this.renderModal()}
                    {this.renderLoginRequiredMessage()}
                </div>
            </Container>
        );
    }
}


export default class Login extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            loginFailed: false,
            isLoginRequired: false,
        };
    }

    render() {
        const { authClient } = this.props;
        const shouldCenter = isMobile();
        const withoutIcon = isMobile();

        return (
            <Nav
                shouldCenter={shouldCenter}
                withoutIcon={withoutIcon}
            >
                <FormExampleForm authClient={authClient} history={this.props.history} />
            </Nav>
        );
    }
}

Login.propTypes = {
    authClient: PropTypes.objectOf(AuthClient).isRequired,
};

// export default MessageExampleAttached;
