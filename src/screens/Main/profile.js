import React, { PureComponent } from 'react';
import { Grid } from 'semantic-ui-react';
import ProfilePicture from '../../components/ProfilePicture';
import Space from '../../components/Space';

const style = {
    container: {
        marginTop: '10px',
    },
    image: {
        height: '100px',
        width: '100px',
    },
    details: {
        marginLeft: '10px',
        marginTop: '10px',
        margin: 'auto',
    },
    detailBlock: {
        display: 'block',
    },
    profile: {
        margin: 'auto',
    },
};

class Profile extends PureComponent {
    renderDetails = () => {
        const { user } = this.props;
        return (
            <Grid columns={2} divided>
                <Grid.Row centered>
                    <Grid.Column width={4} style={{ margin: 'auto', paddingLeft: 20 }}>
                        Name
                    </Grid.Column>
                    <Grid.Column width={8} style={{ margin: 'auto' }}>
                        {`${user.first_name} ${user.last_name}`}
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }

    renderStudentDetail = (user) => {
        const { data } = user;
        const { course } = data;

        return (
            <div>
                {user.email !== '' && (
                    <span style={style.detailBlock}>
                        {' '}
                        <a href={`mailto:${user.email}`} target="_blank">
                            {' '}
                            {user.email}
                            {' '}
                        </a>
                        {' '}
                    </span>
                )}
                <div>
                    <span style={style.detailBlock}>
                        {course.name}
                    </span>
                </div>
                <span style={style.detailBlock}>
                    {course.program.name}
                    (
                    {data.batch}
)
                </span>
                <span style={style.detailBlock}>
                    <a href={data.resume} target="_blank"> Resume</a>
                </span>
            </div>
        );
    }

    renderUserDetail = (user) => {
        const { data, profile } = user;
        if (!profile) return null;
        switch (profile.type) {
        case 'candidate':
            return this.renderStudentDetail(user);
        default:
            return null;
        }
    }

    render() {
        const { user } = this.props;
        const { data } = user;
        const { course } = data;
        return (
            <div style={style.container}>
                <Grid columns={3}>
                    <Grid.Row>
                        <Grid.Column width={4} style={style.profile}>
                            <ProfilePicture />
                        </Grid.Column>
                        <Grid.Column width={8} style={style.details}>
                            <span style={style.detailBlock}>
                                {`${user.first_name} ${user.last_name}`}
                            </span>
                            {this.renderUserDetail(user)}
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <Space height="5px" separate />
                {this.renderDetails()}
            </div>
        );
    }
}

export default Profile;
