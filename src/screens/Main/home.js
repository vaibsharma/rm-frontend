import React, { PureComponent } from 'react';
import {
    Message, Image, Button, Icon, Checkbox, Grid, Header,
} from 'semantic-ui-react';
import ReactHtmlParser from 'react-html-parser';
import showdown from 'showdown';
import RMRow from '../../components/RMRow';
import { isMobile } from '../../utils/helper';
import { GetTime } from '../../utils/constants';

let top = window.screen.height;

if (isMobile()) {
    top = '85vh';
} else {
    top = '85vh';
}

const style = {
    container: {
        paddingTop: 15,
        paddingBottom: 40,
    },
    checkBox: {
        zIndex: 3,
        position: 'fixed',
        right: 10,
        top,
    },
    span: {
        paddingBottom: 10,
        marginRight: 10,
    },
};

const MessageExampleWarningProps = () => (
    <Message
        warning
        header="No feeds yet from your placement center"
        content="For updates please keep an eye on this portal."
    />
);


const renderTextToHtml = (body) => (
    ReactHtmlParser(body)
);

export default class Home extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            toggle: false,
        };
    }

    handleChange = (e, { checked }) => {
        this.setState({
            toggle: checked,
        });
    }

    renderAuthorPicture = (imageUrl) => (
        <Grid.Column
            mobile={3}
            tablet={2}
            computer={2}
            style={{
                margin: 'auto',
                marginTop: '10px',
            }}
        >
            <Image
                src={imageUrl}
                style={{
                    borderRadius: 30,
                    height: 37,
                    width: 37,
                    margin: 'auto',
                }}
            />
        </Grid.Column>
    )

    renderPostMeta = (authorName, date) => (
        <div>
            <span
                style={{
                    fontSize: 13,
                    fontWeight: 900,
                }}
                className="contentMessage"
            >
                { authorName }
            </span>
            <span style={{
                paddingLeft: 5,
                paddingRight: 5,
                color: 'rgba(0,0,0,0.7)',
            }}
            >
                           ·
            </span>
            <span
                style={{
                    color: 'rgba(0,0,0,0.7)',
                    fontSize: 13,
                }}
                className="contentMessage"
            >
                {`${date}`}
            </span>
        </div>
    )

    renderPostHeader = (topicName) => (
        <Header
            className="contentMessage"
            as="h4"
            style={{
                marginTop: 10,
            }}
        >
            {topicName}
        </Header>
    )

    renderPost = (obj) => {
        const converter = new showdown.Converter();
        const { data } = obj;
        const date = GetTime(new Date(data.created_at));
        const { author } = data;
        const authorName = `${author.first_name} ${author.last_name}`;
        const message = renderTextToHtml(converter.makeHtml(data.message));

        return (
            <RMRow>
                {this.renderAuthorPicture('http://localhost:8000/media/avatar/44667288_2194328027492536_895153504927368221_n.jpg')}
                <Grid.Column
                    mobile={13}
                    tablet={14}
                    computer={14}
                    style={{
                        marginLeft: -15,
                    }}
                >
                    {this.renderPostMeta(authorName, date)}
                    {this.renderPostHeader(data.topic_name)}
                    <div style={{
                        overflowX: 'auto',
                    }}
                    >
                        {message}
                    </div>
                </Grid.Column>
            </RMRow>
        );
    }

    renderOpening = (obj) => {
        const { data } = obj;
        const { company, description, position } = data;
        const { name: companyName } = company;
        const date = GetTime(new Date(data.created_at));
        const converter = new showdown.Converter();
        const message = renderTextToHtml(converter.makeHtml(description));

        return (
            <RMRow>
                {this.renderAuthorPicture('http://localhost:8000/media/avatar/44667288_2194328027492536_895153504927368221_n.jpg')}
                <Grid.Column
                    mobile={13}
                    tablet={14}
                    computer={14}
                    style={{
                        marginLeft: -15,
                    }}
                >
                    {this.renderPostMeta(companyName, date)}
                    {this.renderPostHeader(position)}
                    <div style={{
                        overflowX: 'auto',
                    }}
                    >
                        {message}
                    </div>
                </Grid.Column>
                <Grid.Column
                    floated="right"
                    mobile={5}
                    tablet={3}
                    computer={3}
                >
                    <Button secondary>
                        <Icon name="pencil" size="small" />
Apply
                    </Button>
                </Grid.Column>
            </RMRow>
        );
    }

    renderFeedItem = (obj) => {
        if (obj.type === 'post') return this.renderPost(obj);
        if (obj.type === 'opening') return this.renderOpening(obj);
        return null;
    }

    renderFeeds = (homeFeeds) => {
        if (!homeFeeds.data || !homeFeeds.data.filter) return MessageExampleWarningProps();
        const filteredFeed = homeFeeds.data.filter((obj) => {
            const { toggle } = this.state;

            if (toggle) {
                return obj.type === 'opening';
            }
            return true;
        });

        if (filteredFeed.length === 0) {
            return MessageExampleWarningProps();
        }

        return (
            <Grid
                columns={2}
                className="feeds"
                style={{
                    width: '100%',
                }}
            >
                {filteredFeed.map((obj) => this.renderFeedItem(obj))}
            </Grid>
        );
    }

    renderContentType = () => {
        const { userType } = this.props;
        return (userType === 'recruiter'
            && (
                <div style={style.checkBox}>
                    <Checkbox id="green" className="theme green" toggle onClick={this.handleChange} />
                </div>
            )
        );
    }


    render() {
        const { homeFeeds } = this.props;

        return (
            <div style={style.container}>
                {this.renderFeeds(homeFeeds)}
                {/*{this.renderContentType()}*/}
            </div>
        );
    }
}
