import React, { PureComponent } from 'react';
import {
    Button,
    Grid, Header, Image, Label,
    Message, List, Modal, Dropdown, Input,
} from 'semantic-ui-react';
import DateTimePicker from 'react-datetime-picker';
import { capitalizeFirstLetter } from 'normalize-text';
import Icon from 'semantic-ui-react/dist/commonjs/elements/Icon';
import RMRow from '../../components/RMRow';
import { GetTime } from '../../utils/constants';

const MessageExampleWarningProps = () => (
    <Message
        warning
        header="You haven't yet applied to any jobs!"
        content="Check openings to apply."
    />
);

const options = [
    {
        key: 1,
        text: 'Choice 1',
        value: 1,
    },
    {
        key: 2,
        text: 'Choice 2',
        value: 2,
    },
    {
        key: 3,
        text: 'Choice 3',
        value: 3,
    },
];

const ApplicationStatusColor = {
    1: ['Rejected', 'red'],
    2: ['Waiting', 'orange'],
    3: ['On Going', 'yellow'],
    4: ['Completed', 'olive'],
    5: ['Job Offer', 'green'],
};

const ApplicationStatusOptions = [
    {
        key: 1,
        text: 'Rejected',
        value: 1,
    },
    {
        key: 2,
        text: 'Waiting',
        value: 2,
    },
    {
        key: 3,
        text: 'On Going',
        value: 3,
    },
    {
        key: 4,
        text: 'Completed',
        value: 4,
    },
    {
        key: 5,
        text: 'Job Offer',
        value: 5,
    },
];

const style = {
    container: {
        paddingTop: 15,
        paddingBottom: 40,
    },
};


export default class Applications extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            openModal: false,
            application: null,
            date: new Date(),
            interviewDescription: '',
            openStatusConfirmModal: false,
            statusConfirmModal: null,
        };
    }

    componentDidMount() {
        const { userType, interviewers, getInterviewersList } = this.props;
        if (userType === 'interviewer' || userType === 'recruiter') {
            const { data } = interviewers;
            if (data && data.length == 0) {
                getInterviewersList();
            }
        }
    }

    onChangeDescription = (e, { value }) => this.setState({ interviewDescription: value })

    onChangeDate = (date) => this.setState({ date })

    onChangeInterviewer = (event, { value: interviewerId }) => {
        this.setState({
            interviewerId,
        });
    }

    onCloseStatusConfirmModal = () => {
        this.setState({
            openStatusConfirmModal: false,
            statusConfirmModal: null,
        });
    }

    onOpenStatusConfirmModal = (application, status) => {
        this.setState({
            openStatusConfirmModal: true,
            statusConfirmModal: {
                application,
                status,
            },
        });
    }

    renderAvatar = (imageUrl) => (
        <Grid.Column
            mobile={3}
            tablet={2}
            computer={2}
            style={{
                margin: 'auto',
                marginTop: '10px',
            }}
        >
            <Image
                src={imageUrl}
                style={{
                    borderRadius: 30,
                    height: 37,
                    width: 37,
                    margin: 'auto',
                }}
            />
        </Grid.Column>
    )

    renderPostMeta = (application) => {
        const { userType } = this.props;
        const {
            opening: { company }, applied_at: appliedAt, candidate,
        } = application;
        const date = GetTime(new Date(appliedAt));
        let headerName = '';
        if (userType === 'recruiter' || userType === 'interviewer') {
            // eslint-disable-next-line camelcase
            const { first_name, last_name } = candidate;
            // eslint-disable-next-line camelcase
            headerName = `${first_name} ${last_name}`;
        } else {
            headerName = company.name;
        }

        return (
            <div>
                <span
                    style={{
                        fontSize: 13,
                        fontWeight: 900,
                    }}
                    className="contentMessage"
                >
                    {headerName}
                </span>
                <span style={{
                    paddingLeft: 5,
                    paddingRight: 5,
                    color: 'rgba(0,0,0,0.7)',
                }}
                >
                           ·
                </span>
                <span
                    style={{
                        color: 'rgba(0,0,0,0.7)',
                        fontSize: 13,
                    }}
                    className="contentMessage"
                >
                    {`${date}`}
                </span>
            </div>
        );
    }

    renderPostHeader = (topicName) => (
        <Header
            className="contentMessage"
            as="h4"
            style={{
                marginTop: 10,
            }}
        >
            {topicName}
        </Header>
    )

    renderInterview = (interview) => {
        const {
            interviewer: { first_name: firstName, last_name: lastName },
            scheduled_at: scheduledAt,
        } = interview;
        const scheduledTime = GetTime(new Date(scheduledAt));
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column
                        computer={16}
                        mobile={16}
                        tablet={16}
                    >
                        <Header
                            as="h5"
                            style={{
                                color: 'rgba(0,0,0,0.7)',
                            }}
                        >
                            {interview.description}
                        </Header>
                    </Grid.Column>
                    <Grid.Column
                        computer={16}
                        mobile={16}
                        tablet={16}
                    >
                        <List.Description>
                            With
                            { ' ' }
                            <a>
                                <b>
                                    {capitalizeFirstLetter(firstName)}
                                    {' '}
                                    {capitalizeFirstLetter(lastName)}
                                </b>
                            </a>
                            <span style={{
                                paddingLeft: 5,
                                paddingRight: 5,
                                color: 'rgba(0,0,0,0.7)',
                            }}
                            >
                           -
                            </span>
                            <b>
                                {' '}
                                { scheduledTime }
                                {' '}
                            </b>
                        </List.Description>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }

    renderInterviewCheck = (interviews) => {
        if (interviews.length === 0) {
            return (
                <div>
                    {/* eslint-disable-next-line max-len */}
                    <span><i>Your application has been accepted and you will be soon called for the interview</i></span>
                </div>
            );
        }
        return (
            <div>
                {interviews.map((interview) => this.renderInterview(interview))}
            </div>
        );
    }

    // renderDetailButton = (applicationId) => (
    //     <Grid.Column
    //         floated="right"
    //         mobile={7}
    //         tablet={4}
    //         computer={4}
    //         style={{
    //             marginTop: 10,
    //         }}
    //     >
    //         <Button secondary>
    //                 More Details
    //         </Button>
    //     </Grid.Column>
    // )

    renderModal = () => {
        const {
            openModal, application, date, interviewDescription,
        } = this.state;
        const { interviewers } = this.props;
        let interviewersList = [];
        if (interviewers && interviewers.data && interviewers.data.length > 0) {
            interviewersList = interviewers.data.map((obj) => ({
                text: obj.user.name,
                value: obj.id,
                key: obj.id,
            }));
        }
        if (!application) return null;

        const { candidate } = application;
        const candidateName = `${candidate.first_name} ${candidate.last_name}`;

        return (
            <Modal size="small" open={openModal} onClose={this.closeModal}>
                <Modal.Header>Add interviewer</Modal.Header>
                <Modal.Content>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column
                                mobile={4}
                                computer={3}
                                tablet={3}
                                style={{
                                    margin: 'auto',
                                }}
                            >
                                <span><b>Candidate</b></span>
                            </Grid.Column>
                            <Grid.Column
                                mobile={12}
                                computer={13}
                                tablet={13}
                                style={{
                                    margin: 'auto',
                                }}
                            >
                                <span><i>{candidateName}</i></span>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column
                                mobile={4}
                                computer={3}
                                tablet={3}
                                style={{
                                    margin: 'auto',
                                }}
                            >
                                <span><b>Interviewer</b></span>
                            </Grid.Column>
                            <Grid.Column
                                mobile={12}
                                computer={13}
                                tablet={13}
                                style={{
                                    margin: 'auto',
                                }}
                            >
                                <Dropdown clearable options={interviewersList} selection onChange={this.onChangeInterviewer} />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column
                                mobile={4}
                                computer={3}
                                tablet={3}
                                style={{
                                    margin: 'auto',
                                }}
                            >
                                <span><b>Description</b></span>
                            </Grid.Column>
                            <Grid.Column
                                mobile={12}
                                computer={13}
                                tablet={13}
                                style={{
                                    margin: 'auto',
                                }}
                            >
                                <Input
                                    value={interviewDescription}
                                    onChange={this.onChangeDescription}
                                />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column
                                mobile={4}
                                computer={3}
                                tablet={3}
                                style={{
                                    margin: 'auto',
                                }}
                            >
                                <span><b>Time</b></span>
                            </Grid.Column>
                            <Grid.Column
                                mobile={12}
                                computer={13}
                                tablet={13}
                                style={{
                                    margin: 'auto',
                                }}
                            >
                                <DateTimePicker
                                    onChange={this.onChangeDate}
                                    value={date}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Modal.Content>
                <Modal.Actions>
                    <Button
                        negative
                        onClick={this.closeModal}
                    >
Cancel
                    </Button>
                    <Button
                        positive
                        icon="checkmark"
                        labelPosition="right"
                        content="Submit"
                        onClick={this.submitFormRequest}
                    />
                </Modal.Actions>
            </Modal>
        );
    }

    closeModal = () => {
        this.setState({
            openModal: false,
        });
    }

    submitFormRequest = () => {
        const {
            application, interviewerId, date, interviewDescription,
        } = this.state;
        const { createInterview, getMyApplications } = this.props;
        const { id: applicationId, candidate: { data: { id: candidateId } } } = application;

        // console.log(application, interviewerId, date,
        //         //     interviewDescription, candidateId, applicationId);
        createInterview(applicationId, interviewerId, candidateId,
            interviewDescription, date);
        getMyApplications();
        this.closeModal();
    }

    renderAddInterviewerForm = (application) => {
        this.setState({
            openModal: true,
            date: new Date(),
            interviewDescription: '',
            application,
        });
    }

    renderApplicationStatus = (application, status) => {
        const { userType } = this.props;
        let canAddInterviewer = false;
        if (userType === 'recruiter' || userType === 'interviewer') {
            // eslint-disable-next-line camelcase
            canAddInterviewer = true;
        }
        if (!canAddInterviewer) {
            return (
                <Grid.Column
                    floated="right"
                    mobile={7}
                    tablet={4}
                    computer={4}
                >
                    <Label size="large" horizontal color={ApplicationStatusColor[status][1]}>
                        {ApplicationStatusColor[status][0]}
                    </Label>
                </Grid.Column>
            );
        }
        return (
            <Grid.Column
                mobile={16}
                tablet={8}
                computer={8}
                style={{
                    marginLeft: 0,
                }}
            >
                {/* <Label size="large" horizontal color={ApplicationStatusColor[status][1]}> */}
                {/*    {ApplicationStatusColor[status][0]} */}
                {/* </Label> */}
                <Dropdown
                    placeholder="Status"
                    search
                    defaultValue={status}
                    selection
                    onChange={
                        (state, { value }) => this.onOpenStatusConfirmModal(application, value)
                    }
                    options={ApplicationStatusOptions}
                />
            </Grid.Column>
        );
    }

    renderApplicationMeta = (application) => {
        const { userType } = this.props;
        const { status } = application;
        let canAddInterviewer = false;
        if (userType === 'recruiter' || userType === 'interviewer') {
            // eslint-disable-next-line camelcase
            canAddInterviewer = true;
        }

        return (
            <Grid style={{
                width: '100%',
                marginTop: 10,
            }}
            >
                {canAddInterviewer && (
                    <Grid.Column
                        mobile={10}
                        tablet={8}
                        computer={8}
                    >
                        <Button
                            onClick={() => this.renderAddInterviewerForm(application)}
                        >
                            <Icon name="add user" size="small" />
                            Add Interviewer
                        </Button>
                    </Grid.Column>
                )}
                {this.renderApplicationStatus(application, status)}
            </Grid>
        );
    }


    renderApplication = (application) => {
        const {
            opening, interviews,
        } = application;

        return (
            <RMRow>
                {this.renderAvatar('http://localhost:8000/media/avatar/44667288_2194328027492536_895153504927368221_n.jpg')}
                <Grid.Column
                    mobile={13}
                    tablet={14}
                    computer={14}
                    style={{
                        marginLeft: -15,
                    }}
                >
                    {this.renderPostMeta(application)}
                    {this.renderPostHeader(opening.position)}
                    {this.renderInterviewCheck(interviews)}
                    {this.renderApplicationMeta(application)}
                </Grid.Column>
            </RMRow>
        );
    }

    renderApplicationList = () => {
        const { applications: { data } } = this.props;
        if (!data || data.length === 0) {
            return MessageExampleWarningProps();
        }
        return (
            <Grid
                columns={2}
                className="feeds"
                style={{
                    width: '100%',
                }}
            >
                {data.map((obj) => this.renderApplication(obj))}
            </Grid>
        );
    }

    updateApplicationStatusFromModal = (applicationId, status) => {
        const { getMyApplications, updateApplicationStatus } = this.props;
        updateApplicationStatus(applicationId, status);
        this.onCloseStatusConfirmModal();
        getMyApplications();
    }


    renderStatusConfirmModal = () => {
        const { openStatusConfirmModal, statusConfirmModal } = this.state;
        const { updateApplicationStatus } = this.props;
        if (!statusConfirmModal) {
            return null;
        }
        const { application: { id: applicationId, status: previousStatus }, status } = statusConfirmModal;
        return (
            <Modal size="mini" open={openStatusConfirmModal} onClose={this.onCloseStatusConfirmModal}>
                <Modal.Header>Change Status</Modal.Header>
                <Modal.Content>
                    <p>{'Are you sure you want to change this application\'s status?'}</p>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column
                                mobile={8}
                                computer={8}
                                tablet={8}
                            >
                                <span><b>Previous Status</b></span>
                            </Grid.Column>
                            <Grid.Column
                                mobile={8}
                                computer={8}
                                tablet={8}
                            >
                                <Label
                                    size="large"
                                    horizontal
                                    color={ApplicationStatusColor[previousStatus][1]}
                                >
                                    {ApplicationStatusColor[previousStatus][0]}
                                </Label>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column
                                mobile={8}
                                computer={8}
                                tablet={8}
                            >
                                <span><b>New Status</b></span>
                            </Grid.Column>
                            <Grid.Column
                                mobile={8}
                                computer={8}
                                tablet={8}
                            >
                                <Label
                                    size="large"
                                    horizontal
                                    color={ApplicationStatusColor[status][1]}
                                >
                                    {ApplicationStatusColor[status][0]}
                                </Label>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Modal.Content>
                <Modal.Actions>
                    <Button
                        negative
                        onClick={this.onCloseStatusConfirmModal}
                    >
Cancel
                    </Button>
                    <Button
                        onClick={() => { this.updateApplicationStatusFromModal(applicationId, status); }}
                        positive
                        icon="checkmark"
                        labelPosition="right"
                        content="Yes"
                    />
                </Modal.Actions>
            </Modal>
        );
    }


    render() {
        return (
            <div style={style.container}>
                {this.renderApplicationList()}
                {this.renderModal()}
                {this.renderStatusConfirmModal()}
            </div>
        );
    }
}
