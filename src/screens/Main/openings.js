import React, { PureComponent } from 'react';
import {
    Message, Image, Label, Button, Icon, Grid, Header,
} from 'semantic-ui-react';
import showdown from 'showdown';
import ReactHtmlParser from 'react-html-parser';
import { GetTime } from '../../utils/constants';
import RMRow from '../../components/RMRow';

const MessageExampleWarningProps = () => (
    <Message
        warning
        header="No interviews lined up for you currently"
        content="Keep applying!"
    />
);

const renderTextToHtml = (body) => (
    ReactHtmlParser(body)
);

const style = {
    container: {
        paddingTop: 15,
        paddingBottom: 40,
    },
    checkBox: {
        position: 'fixed',
        right: 10,
    },
    span: {
        paddingBottom: 10,
        marginRight: 10,
    },
};


export default class Opening extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isFailed: false,
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        const { openings: { applyFailed: isFailed } } = this.props;
        const { openings: { applyFailed: newIsFailed } } = nextProps;
        if (!isFailed && newIsFailed) {
            this.setState({
                isFailed: true,
            }, () => {
                setTimeout(() => {
                    this.setState({
                        isFailed: false,
                    });
                }, 3000);
            });
        }
    }

    renderAuthorPicture = (imageUrl) => (
        <Grid.Column
            mobile={3}
            tablet={2}
            computer={2}
            style={{
                margin: 'auto',
                marginTop: '10px',
            }}
        >
            <Image
                src={imageUrl}
                style={{
                    borderRadius: 30,
                    height: 37,
                    width: 37,
                    margin: 'auto',
                }}
            />
        </Grid.Column>
    )

    canApply = (obj) => {
        const { applications: { data } } = this.props;
        if (!data) return true;
        for (let x = 0; x < data.length; x++) {
            if (data[x].opening.id == obj.data.id) return false;
        }
        return true;
    }

    renderPostMeta = (authorName, date) => (
        <div>
            <span
                style={{
                    fontSize: 13,
                    fontWeight: 900,
                }}
                className="contentMessage"
            >
                {authorName}
            </span>
            <span style={{
                paddingLeft: 5,
                paddingRight: 5,
                color: 'rgba(0,0,0,0.7)',
            }}
            >
                           ·
            </span>
            <span
                style={{
                    color: 'rgba(0,0,0,0.7)',
                    fontSize: 13,
                }}
                className="contentMessage"
            >
                {`${date}`}
            </span>
        </div>
    )

    renderPostHeader = (topicName) => (
        <Header
            className="contentMessage"
            as="h4"
            style={{
                marginTop: 10,
            }}
        >
            {topicName}
        </Header>
    )

    renderFeedItem = (obj) => {
        if (obj.type === 'opening') return this.renderOpening(obj);
        return null;
    }

    renderEligibleCourses = (courses) => {
        if (!courses) return null;
        return (
            <div
                style={{
                    paddingTop: 2,
                }}
            >
                {courses.map((obj) => (
                    <Label
                        as="a"
                        basic
                        color="blue"
                        style={{
                            marginBottom: 5,
                        }}
                    >
                        {obj.name}
                    </Label>
                ))}
            </div>
        );
    }

    renderCutOff = (cutoff) => (
        <div>
            <span>
                <b>Cutoff</b>
                { ' - '}
                {cutoff}
            </span>
        </div>
    )

    renderOpening = (obj) => {
        const { openings, openings: { meta }, userType } = this.props;
        const { data } = obj;
        const {
            company, description, position, eligible_courses, cutoff,
        } = data;
        const { name: companyName } = company;
        const date = GetTime(new Date(data.created_at));
        const converter = new showdown.Converter();
        const message = renderTextToHtml(converter.makeHtml(description));
        const canApply = this.canApply(obj);
        const openingId = data.id;
        const isApplyLoading = (openings.applyLoading && (data.id == meta.opening_id));
        const isApplyFailed = (this.state.isFailed && (data.id == meta.opening_id));

        return (
            <RMRow>
                {this.renderAuthorPicture('http://localhost:8000/media/avatar/44667288_2194328027492536_895153504927368221_n.jpg')}
                <Grid.Column
                    mobile={13}
                    tablet={14}
                    computer={14}
                    style={{
                        marginLeft: -15,
                    }}
                >
                    {this.renderPostMeta(companyName, date)}
                    {this.renderPostHeader(position)}
                    <div style={{
                        overflowX: 'auto',
                    }}
                    >
                        {message}
                    </div>
                    {this.renderEligibleCourses(eligible_courses)}
                    {this.renderCutOff(cutoff)}
                </Grid.Column>
                { (userType !== 'recruiter' && userType !== 'interviewer') && (
                    <Grid.Column
                        floated="right"
                        mobile={6}
                        tablet={3}
                        computer={3}
                    >
                        {/* eslint-disable-next-line max-len */}
                        <Button
                            secondary
                            disabled={!canApply}
                            loading={isApplyLoading}
                            onClick={() => { this.props.applyOpening(openingId); }}
                        >
                            <Icon name="pencil" size="small" />
                            {canApply && 'Apply'}
                            {!canApply && 'Applied'}
                        </Button>
                    </Grid.Column>
                ) }
                {isApplyFailed && (
                    <Grid.Column
                        mobile={16}
                        computer={16}
                        tablet={16}
                    >
                        <Label
                            style={{
                                width: '100%',
                                margin: 'auto',
                                textAlign: 'center',
                                marginTop: 5,
                            }}
                            color="grey"
                            size="large"
                        >
                            {' '}
                            {'You can\'t apply to this opening currently.'}
                            {' '}
                        </Label>
                    </Grid.Column>
                )}
            </RMRow>
        );
    }

    renderOpeningList = () => {
        const { openings: { data } } = this.props;
        if (!data || data.length == 0) {
            return MessageExampleWarningProps();
        }

        return (
            <Grid
                columns={2}
                className="feeds"
                style={{
                    width: '100%',
                }}
            >
                {data.map((obj) => this.renderFeedItem(obj))}
            </Grid>
        );
    }


    render() {
        return (
            <div style={style.container}>
                {this.renderOpeningList()}
            </div>
        );
    }
}
