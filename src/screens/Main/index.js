import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Screen from '../../components/Screen';
import Footer from '../../components/Footer';
import Profile from './profile';
import Home from './home';
import Opening from './openings';
import Applications from './application';
import { TABS } from '../../utils/constants';
import { getProfile } from '../../actions/user';
import { getHomeFeed } from '../../actions/feeds';
import { getMyApplications, updateApplicationStatus } from '../../actions/application';
import { getOpenings, applyOpening } from '../../actions/openings';
import { getInterviewersList, createInterview } from '../../actions/interviewers';


class Main extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            iconActive: TABS.HOME,
        };
    }

    componentDidMount() {
        this.refresh();
    }

    refresh = () => {
        // eslint-disable-next-line no-shadow,react/prop-types
        const {
            actions: {
                getProfile, getHomeFeed, getMyApplications, getOpenings,
            },
        } = this.props;
        getProfile();
        getHomeFeed();
        getMyApplications();
        getOpenings();
    }

    applyOpening = (openingId) => {
        const { actions: { applyOpening } } = this.props;
        applyOpening(openingId);
    }

    renderContent = () => {
        const { iconActive } = this.state;
        const {
            user, homeFeeds, myApplication: applications, openings, interviewers,
        } = this.props;
        const {
            actions: {
                getInterviewersList, createInterview, getMyApplications, updateApplicationStatus,
            },
        } = this.props;
        const { profile: { type } } = user;

        switch (iconActive) {
        case TABS.HOME:
            return (
                <Home
                    userType={type}
                    homeFeeds={homeFeeds}
                    refresh={this.refresh}
                />
            );
        case TABS.PROFILE:
            return (
                <Profile
                    user={user}
                />
            );
        case TABS.OPENING:
            return (
                <Opening
                    userType={type}
                    openings={openings}
                    applications={applications}
                    refresh={this.refresh}
                    applyOpening={this.applyOpening}
                />
            );
        case TABS.APPLICATIONS:
            return (
                <Applications
                    updateApplicationStatus={updateApplicationStatus}
                    getMyApplications={getMyApplications}
                    userType={type}
                    applications={applications}
                    interviewers={interviewers}
                    getInterviewersList={getInterviewersList}
                    createInterview={createInterview}
                    refresh={this.refresh}
                />
            );
        default:
            return null;
        }
    }

    onClickWithVal = (iconActive = TABS.HOME) => {
        this.setState({
            iconActive,
        });
    }


    renderFooter = () => {
        const { iconActive } = this.state;

        return (
            <Footer
                onClickTab={this.onClickWithVal}
                iconActive={iconActive}
            />
        );
    }

    logout = () => {
        this.props.authClient.logout((data) => {
            console.log('logout successful', data);
        }, () => {
            console.log('logout failed');
        });
    }

    render() {
        const { authClient: { isAuthenticated } } = this.props;
        return (
            <Screen
                footer={this.renderFooter()}
                content={this.renderContent()}
                logout={this.logout}
                isAuthenticated={isAuthenticated()}
            />
        );
    }
}

const mapStateToProps = (state) => {
    const { user } = state;
    return (state);
};

export default connect(mapStateToProps, (dispatch, state) => ({
    actions: bindActionCreators({
        getProfile,
        getHomeFeed,
        getMyApplications,
        getOpenings,
        applyOpening,
        getInterviewersList,
        createInterview,
        updateApplicationStatus,
    }, dispatch),
}))(Main);
