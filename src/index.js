import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import './index.css';
import todoApp from './reducers';
import Root from './components/root';

import * as serviceWorker from './serviceWorker';

let middleware = [
    thunk,
]

if (process.env.NODE_ENV !== 'production') {
    // eslint-disable-next-line global-require
    const logger = require('redux-logger');

    middleware = [...middleware, logger.logger];
}


const store = createStore(todoApp, applyMiddleware(...middleware));

const cssId = 'myCss'; // you could encode the css path itself to generate id..
if (!document.getElementById(cssId)) {
    const head = document.getElementsByTagName('head')[0];
    const link = document.createElement('link');
    link.id = cssId;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = 'https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css';
    link.media = 'all';
    head.appendChild(link);
}

const fontId = 'font-rubik'; // you could encode the css path itself to generate id..
if (!document.getElementById(fontId)) {
    const head = document.getElementsByTagName('head')[0];
    const link = document.createElement('link');
    link.id = fontId;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = 'https://fonts.googleapis.com/css?family=Rubik&display=swap';
    link.media = 'all';
    head.appendChild(link);
}

render(<Root store={store} />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
