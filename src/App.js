// eslint-disable-next-line max-classes-per-file
import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    BrowserRouter as Router, Route, Switch,
} from 'react-router-dom';
import PropTypes from 'prop-types';
import AuthClient from './utils/auth';
import { emptyDom } from './utils/helper';
import Login from './screens/Login';
import Main from './screens/Main';
import './App.css';

function Authentication(props) {
    if (props.location.pathname === '/login') {
        if (props.authActions.isAuthenticated()) {
            props.history.push('/');
        } else {
            return emptyDom();
        }
    }

    if (!props.authActions.isAuthenticated()) {
        props.authActions.verifyAndDoNeedful((check) => {
        }, (error) => {
            const { isLoginRequired, loginFailed } = error;
            if (isLoginRequired || loginFailed) {
                props.history.push(`/login?login=true&url=${window.location.pathname}`, {
                    isLoginRequired,
                    loginFailed,
                });
            }
        });
    }

    return emptyDom();
}

class AuthLayer extends PureComponent {
    componentDidMount() {
        const { authClient } = this.props;

        authClient.reinitializeAuth();
    }

    render() {
        return null;
    }
}

AuthLayer.propTypes = {
    authClient: PropTypes.objectOf(AuthClient).isRequired,
};

class App extends PureComponent {
    render() {
        const { dispatch } = this.props;
        const authClient = new AuthClient(dispatch);

        return (
            <Router>
                <Route
                    exact
                    component={(props) => (
                        <Authentication
                            authActions={authClient}
                            {...props}
                        />
                    )}
                />
                <AuthLayer authClient={authClient} />
                <Switch>
                    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                    <Route path="/login" component={(props) => <Login authClient={authClient} {...props} />} />
                    <Route exact path="/" component={(props) => <Main authClient={authClient} {...props}/>} />
                </Switch>
            </Router>
        );
    }
}

App.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = ({ auth }) => ({
    auth,
});

export default connect(mapStateToProps, (dispatch, ownProps) => ({
    actions: bindActionCreators({
        // getAccessToken: actions.getAccessToken,
    }, dispatch),
    dispatch,
}))(App);
