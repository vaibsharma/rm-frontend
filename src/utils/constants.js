import {
    formatRelative
} from 'date-fns';


const BASE_URL = process.env.NODE_ENV === 'production' ? 'https://abchd.com' : 'localhost:3000';
const API_URL = process.env.NODE_ENV === 'production' ? 'https://abchd.com' : 'localhost:8000';
const CLIENT_ID = 'mQSTGtjbG3BXOnulJrjMA1sO82wtrdUmoDaUTVZ1';
const CLIENT_SECRET = 'nXzIUq8kAduyPuI09TOC2Z0ZJImgHodtd9Rr4vudzkDRiTDsi3SRSfEJBfsGR9N2Ecorb9AzK0uTcMeTSDKpDDg3LoGl3cnbO0LixvIZamRfaMcctcHLaX0rRhMiGvWL';

const CLIENT_CREDENTIALS = 'password';
const REFRESH_TOKEN = 'refresh_token';
const HOME = 'HOME';
const OPENING = 'OPENING';
const PROFILE = 'PROFILE';
const APPLICATIONS = 'APPLICATIONS';


const TABS = {
    HOME,
    OPENING,
    PROFILE,
    APPLICATIONS,
};

const DAYS = {
    1: 'Monday',
    2: 'Tuesday',
    3: 'Wednesday',
    4: 'Thursday',
    5: 'Friday',
    6: 'Saturday',
    0: 'Sunday',
}

const GetTime = (date) => {
    const todayDate = new Date();
    return formatRelative(date, todayDate, { weekStartsOn: 0 });
}


export {
    BASE_URL,
    CLIENT_ID,
    CLIENT_SECRET,
    CLIENT_CREDENTIALS,
    REFRESH_TOKEN,
    API_URL,
    TABS,
    DAYS,
    GetTime,
};
