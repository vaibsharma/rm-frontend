import Cookies from 'js-cookie';
import AUTH from '../actions/auth';

import requests from './requests';

import {
    CLIENT_ID, CLIENT_SECRET, CLIENT_CREDENTIALS, REFRESH_TOKEN,
} from './constants';
import urls from './urls';

class AuthClient {
    constructor(dispatch) {
        this.accessToken = Cookies.get('accessToken');
        this.refreshToken = Cookies.get('refreshToken');
        this.expiresAt = Cookies.get('expiresAt');
        this.dispatch = dispatch;
        // this.accessToken = this.accessToken.bind(this);
    }

    reinitializeAuth(init = false) {
        const [accessToken, refreshToken, expiresAt] = [Cookies.get('accessToken'), Cookies.get('refreshToken'), Cookies.get('expiresAt')];
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.expiresAt = expiresAt;
        let dispatch = {
            data: {
                accessToken,
                refreshToken,
                expiresAt,
            },
        };
        if (init) {
            dispatch = {
                ...dispatch,
                type: AUTH.INIT,
            };
        } else {
            dispatch = {
                ...dispatch,
                type: AUTH.SUCCESSFUL,
            };
        }
        if (this.accessToken == 'undefined') {
            dispatch = {
                ...dispatch,
                type: AUTH.FAILED,
            };
        }
        this.dispatch(dispatch);
    }

    isExpired() {
        const currentTime = new Date().getTime();
        return (this.expiresAt && this.expiresAt < currentTime);
    }

    isAuthenticated = () => {
        if (this.accessToken === 'undefined' || !this.accessToken || this.isExpired()) return false;
        return true;
    }

    resetAuth() {
        Cookies.remove('accessToken');
        Cookies.remove('refreshToken');
        Cookies.remove('expiresAt');
        this.reinitializeAuth();
    }

    updateSession(authData) {
        // Set the time that the access token will expire at
        // eslint-disable-next-line camelcase
        const { access_token, refresh_token, expires_in } = authData;
        // eslint-disable-next-line camelcase
        const expiresAt = JSON.stringify((expires_in * 1000) + new Date().getTime());
        Cookies.set('accessToken', access_token);
        Cookies.set('refreshToken', refresh_token);
        Cookies.set('accessToken', access_token);
        Cookies.set('expiresAt', expiresAt);
        this.reinitializeAuth();
    }

    // eslint-disable-next-line class-methods-use-this
    generateNewToken(username, password, onSuccessCallback = () => {}, onErrorCallback = () => {}) {
        const body = {
            grant_type: CLIENT_CREDENTIALS,
            client_secret: CLIENT_SECRET,
            client_id: CLIENT_ID,
            username,
            password,
        };
        console.log("generating new token");

        return requests.post(urls.get('REFRESH_TOKEN'), body, false)
            .then((res) => {
                if(res.error){
                    onErrorCallback({ loginFailed: true });
                } else {
                    this.updateSession(res);
                    onSuccessCallback(res);
                }
            })
            .catch((error) => {
                onErrorCallback({ loginFailed: true });
            });
    }

    updateAccessToken(onSuccessCallback = () => {}, onErrorCallback = () => {}) {
        const body = {
            grant_type: REFRESH_TOKEN,
            client_secret: CLIENT_SECRET,
            client_id: CLIENT_ID,
            refresh_token: this.refreshToken,
        };
        return requests.post(urls.get('REFRESH_TOKEN'), body, false)
            .then((res) => {
                if (res.error) {
                    throw res.error;
                }
                this.updateSession(res);
                onSuccessCallback(res);
            }).catch((err) => {
                onErrorCallback({ loginFailed: true });
            });
    }

    verifyAndDoNeedful(onSuccessCallback = () => {}, onErrorCallback = () => {}) {
        if (!this.isAuthenticated()) {
            if ((this.refreshToken !== '' && this.refreshToken !== 'undefined' && this.refreshToken)) {
                this.updateAccessToken(onSuccessCallback, onErrorCallback);
            } else {
                onErrorCallback({ isLoginRequired: true });
            }
            return;
        }
        onSuccessCallback({ isLogin: true });
    }

    login(username, password, onSuccessCallback = () => {}, onErrorCallback = () => {}) {
        this.generateNewToken(username, password, onSuccessCallback, onErrorCallback);
    }

    logout = (onSuccessCallback = () => {}, onErrorCallback = () => {}) => {
        const body = {
            token: this.accessToken,
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
        };
        return requests.post(urls.get('REVOKE_TOKEN'), body, false)
            .then((res) => {
                this.resetAuth();
                onSuccessCallback(res);
            })
            .catch((err) => {
                this.resetAuth();
                onErrorCallback();
            });
    }
}


export default AuthClient;
