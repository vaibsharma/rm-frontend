import { API_URL } from './constants';

const REFRESH_TOKEN = 'oauth2/token/';

const REVOKE_TOKEN = 'oauth2/revoke_token/';

const URLS = {
    REFRESH_TOKEN,
    REVOKE_TOKEN,
};

const get = (url, defaultValue = undefined) => {
    if (URLS[url]) return `http://${API_URL}/${URLS[url]}`;

    if (defaultValue) {
        return `http://${API_URL}/${defaultValue}`;
    }

    return defaultValue;
};

export default {
    REFRESH_TOKEN,
    REVOKE_TOKEN,
    get,
};
