import React from 'react';
import { isMobile as checkMobile, isTablet } from 'react-device-detect';

const emptyDom = (props = {}) => {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <div {...props} />;
}

const isMobile = () => {
    if (isTablet) return false;
    return (checkMobile);
}

export {
    emptyDom,
    isMobile,
};
