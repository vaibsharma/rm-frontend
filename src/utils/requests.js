import Cookie from 'js-cookie';

const POST = 'POST';
const GET = 'GET';
const PUT = 'PUT';

const methods = {
    POST,
    GET,
    PUT,
};

const defaultHeaders = {
    'Content-Type': 'application/json',
};

const getHeaders = (fromUser = false) => {
    let headers = defaultHeaders;

    if (fromUser) {
        const accessToken = Cookie.get('accessToken');

        headers = {
            ...headers,
            Authorization: `Bearer ${accessToken}`,
        };
    }
    return headers;
};

const get = (url) => {
    const headers = getHeaders(true);

    return fetch(url, {
        method: GET,
        headers,
        data: '',
    }).then((res) => res.json());
};

const post = (url, body, fromUser = true) => {
    // console.log(url, headers, body);
    const headers = getHeaders(fromUser);

    return fetch(url, {
        method: POST,
        headers,
        body: JSON.stringify(body),
    })
        .then((response) => {
            return response.json()
                .then((data) => {
                    if (!response.ok) {
                        throw Error(data.err || 'HTTP error');
                    }
                    return data;
                });
        });
};

export default {
    methods,
    get,
    post,
};
