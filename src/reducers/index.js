import { combineReducers } from 'redux';
import todos from './todos';
import auth from './auth';
import user from './user';
import homeFeeds from './feeds';
import myApplication from './applications';
import openings from './openings';
import interviewers from './interviewers';

export default combineReducers({
    todos,
    auth,
    user,
    homeFeeds,
    myApplication,
    openings,
    interviewers,
});
