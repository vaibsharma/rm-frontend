import OPENINGS, { APPLY_OPENING } from '../actions/openings';

const STATE = OPENINGS.state;

const defaultState = {
    data: [],
    meta: {},
    applyLoading: false,
    appplyLoaded: false,
    applyFailed: false,
    isLoading: false,
    isLoaded: false,
    isFailed: false,
};

const openings = (state = defaultState, action) => {
    switch (action.type) {
    case STATE.INIT:
        return {
            ...state,
            data: action.data,
            isLoading: true,
            isFailed: false,
        };
    case STATE.SUCCESSFUL:
        return {
            ...state,
            data: action.data,
            isLoading: false,
            isLoaded: true,
        };
    case STATE.FAILED:
        return {
            ...state,
            data: action.data,
            isLoading: false,
            isFailed: true,
        };
    case APPLY_OPENING.INIT:
        return {
            ...state,
            meta: {
                ...state.meta,
                ...action.data,
            },
            applyLoading: true,
            applyFailed: false,
        };
    case APPLY_OPENING.SUCCESSFUL:
        return {
            ...state,
            meta: {
                ...state.meta,
                ...action.data,
            },
            applyLoaded: true,
            applyLoading: false,
        };
    case APPLY_OPENING.FAILED:
        return {
            ...state,
            meta: {
                ...state.meta,
                ...action.data,
            },
            applyLoading: false,
            applyFailed: true,
        };
    default:
        return state;
    }
};

export default openings;
