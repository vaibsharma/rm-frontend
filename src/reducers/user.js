import USERACTIONS from "./../actions/user";

const STATE = USERACTIONS.state;

const defaultState = {
    username: null,
    first_name: null,
    last_name: null,
    email: null,
    profile: {
    },
    data: {
    },
    error: null,
};

const user = (state = defaultState, action) => {
    switch (action.type) {
    case STATE.INIT:
        return {
            ...defaultState,
            ...action.data,
            isLoading: true,
        };
    case STATE.SUCCESSFUL:
        return {
            ...defaultState,
            ...action.data,
            isLoaded: true,
        };
    case STATE.FAILED:
        return {
            ...defaultState,
            ...action.data,
            isFailed: true,
        };
    default:
        return state;
    }
};

export default user;
