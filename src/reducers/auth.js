import AUTH from '../actions/auth';

const defaultState = {
    accessToken: undefined,
    refreshToken: undefined,
    expiresAt: null,
    isLoaded: false,
    isLoading: false,
    isFailed: false,
};

const auth = (state = defaultState, action) => {
    switch (action.type) {
    case AUTH.INIT:
        return {
            ...defaultState,
            ...action.data,
            isLoading: true,
        };
    case AUTH.SUCCESSFUL:
        return {
            ...defaultState,
            ...action.data,
            isLoaded: true,
        };
    case AUTH.FAILED:
        return {
            ...defaultState,
            ...action.data,
            isFailed: true,
        };
    default:
        return state;
    }
};

export default auth;
