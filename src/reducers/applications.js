import MY_APPLICATION from '../actions/application';

const STATE = MY_APPLICATION.state;

const defaultState = {
    data: [],
    isLoading: false,
    isLoaded: false,
    isFailed: false,
};

const myApplications = (state = defaultState, action) => {
    switch (action.type) {
    case STATE.INIT:
        return {
            ...defaultState,
            data: action.data,
            isLoading: true,
        };
    case STATE.SUCCESSFUL:
        return {
            ...defaultState,
            data: action.data,
            isLoaded: true,
        };
    case STATE.FAILED:
        return {
            ...defaultState,
            data: action.data,
            isFailed: true,
        };
    default:
        return state;
    }
};

export default myApplications;
