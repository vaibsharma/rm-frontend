import HOME_FEED from '../actions/feeds';

const STATE = HOME_FEED.state;

const defaultState = {
    data: [],
    isLoading: false,
    isLoaded: false,
    isFailed: false,
};

const homeFeeds = (state = defaultState, action) => {
    switch (action.type) {
    case STATE.INIT:
        return {
            ...defaultState,
            data: action.data,
            isLoading: true,
        };
    case STATE.SUCCESSFUL:
        return {
            ...defaultState,
            data: action.data,
            isLoaded: true,
        };
    case STATE.FAILED:
        return {
            ...defaultState,
            data: action.data,
            isFailed: true,
        };
    default:
        return state;
    }
};

export default homeFeeds;
