import { INTERVIEWS_LIST } from '../actions/interviewers';

const STATE = INTERVIEWS_LIST;

const defaultState = {
    data: [],
    meta: {},
    isLoading: false,
    isLoaded: false,
    isFailed: false,
};

const interviewers = (state = defaultState, action) => {
    switch (action.type) {
    case STATE.INIT:
        return {
            ...state,
            data: action.data,
            isLoading: true,
            isFailed: false,
        };
    case STATE.SUCCESSFUL:
        return {
            ...state,
            data: action.data,
            isLoading: false,
            isLoaded: true,
        };
    case STATE.FAILED:
        return {
            ...state,
            data: action.data,
            isLoading: false,
            isFailed: true,
        };
    default:
        return state;
    }
};

export default interviewers;
